Left image viewer: an image viewing program.
-----------------------------------------------------

Description
-----------

This is the Left image viewer, an image viewer program.  It is meant to be
a fast and minimalistic image viewer writing on iced-rs and gtk-rs.

License
-------

This program is released under the terms of the GNU General Public
License.  Please see the file LICENSE for details.
