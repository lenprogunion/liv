use iced::widget::button::{Appearance, StyleSheet};
use iced::{
    color,
    BorderRadius
};

pub struct TitleBarButton;

impl StyleSheet for TitleBarButton {
    type Style = iced::Theme;
    
    fn active(&self, _style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from(13.0),
            background: Some(iced::Background::Color(color!(44, 44, 44))),
            ..Appearance::default()
        }
    }
    fn hovered(&self, _style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from(13.0),
            background: Some(iced::Background::Color(color!(44, 44, 44, 0.7))),
            ..Appearance::default()
        }
    }
    fn pressed(&self, _style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from(13.0),
            background: Some(iced::Background::Color(color!(44, 44, 44, 0.9))),
            ..Appearance::default()
        }
    }
}

pub struct TitleBarMenuButton;

impl StyleSheet for TitleBarMenuButton {
    type Style = iced::Theme;
    
    fn active(&self, _style: &Self::Style) -> Appearance {
        Appearance{
            background: Some(iced::Background::Color(color!(44, 44, 44))),
            ..Appearance::default()
        }
    }
    fn hovered(&self, _style: &Self::Style) -> Appearance {
        Appearance{
            background: Some(iced::Background::Color(color!(44, 44, 44, 0.7))),
            ..Appearance::default()
        }
    }
    fn pressed(&self, _style: &Self::Style) -> Appearance {
        Appearance{
            background: Some(iced::Background::Color(color!(44, 44, 44, 0.9))),
            ..Appearance::default()
        }
    }
}