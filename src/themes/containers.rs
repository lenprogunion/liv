use iced::widget::container::{Appearance, StyleSheet};
use iced::{
    color, 
    BorderRadius
};

pub struct ContainerTheme;

impl StyleSheet for ContainerTheme {
    type Style = iced::Theme;
    
    fn appearance(&self, _style: &Self::Style) -> Appearance {
        Appearance {
            text_color: Some(color!(44,44,44)),
            background: Some(iced::Background::Color(color!(255, 255, 255))),
            border_radius: BorderRadius::from(20.0),
            border_width: 0.0,
            border_color: color!(0,0,0,0.0)
        }
    }
}

pub struct ContainerFullscreanTheme;

impl StyleSheet for ContainerFullscreanTheme {
    type Style = iced::Theme;
    
    fn appearance(&self, _style: &Self::Style) -> Appearance {
        Appearance {
            text_color: Some(color!(44,44,44)),
            background: Some(iced::Background::Color(color!(255, 255, 255))),
            border_radius: BorderRadius::from(0.0),
            border_width: 0.0,
            border_color: color!(0,0,0,0.0)
        }
    }
}

pub struct TitlebarTheme;

impl StyleSheet for TitlebarTheme {
    type Style = iced::Theme;
    
    fn appearance(&self, _style: &Self::Style) -> Appearance {
        Appearance {
            text_color: Some(color!(44,44,44)),
            background: Some(iced::Background::Color(color!(255, 255, 255))),
            border_radius: BorderRadius::from([20.0, 20.0, 0.0, 0.0]),
            border_width: 0.0,
            border_color: color!(0,0,0,0.0)
        }
    }
}