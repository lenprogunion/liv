use iced_aw::style::modal::{StyleSheet, Appearance};
use iced::{
    color, 
};

pub struct ModalTheme;

impl StyleSheet for ModalTheme {
    type Style = iced::Theme;
    
    fn active(&self, _style: &Self::Style) -> Appearance {
        Appearance {
            background: iced::Background::Color(color!(255, 255, 255, 0.0)),
        }
    }
}