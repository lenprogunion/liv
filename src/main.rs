use std::env;

use image;

use iced::{self, 
    Settings, 
    Application, 
    Subscription, subscription, Command, Event,
    executor, 
    widget::{
        container, 
        text,
        image::{Viewer, Handle},
        column, row,
        toggler
    }, 
    window, 
    Length, Size,
};

use iced_aw::{
    modal,
    floating_element::Anchor,
    floating_element
};

mod widgets;
mod themes;
mod manager;

use manager::files::{self, create_settings_file, create_config_dir};


struct Liv {
    current_image: Option<usize>,
    current_folder: Option<Vec<String>>,
    folder_length: usize,
    is_hidden: bool,
    settings: manager::settings_parser::Settings,
    path_to_settings_file: String
}

#[derive(Debug, Clone)]
pub enum Message {
    Close,
    ChangeImage(usize),
    DragWindow,
    ToggleImageInDirectory(Event),
    ToggleModal,
    ToggleTitlebar(bool)
}   

impl Application for Liv {
    type Executor = executor::Default;
    type Theme = iced::Theme;
    type Message = Message;
    type Flags = ();
    fn new(_flags: Self::Flags) -> (Self, Command<Self::Message>) {
        let args: Vec<String> = env::args().collect();

        let (
            folder_length,
            current_image, 
            current_folder,
            image_size
        ) = if args.len() == 1 {
            (0, None, None, Size::new(500, 300))
        } else {
            let folder = files::view_directory(args[1].clone());
            let mut current_index = 0;
            for index in 0..folder.len() {
                if folder[index] == args[1].clone() {
                    current_index = index;
                    break;
                }
            }
            let image = image::open(folder[current_index].clone()).unwrap().to_rgb16();
            let image_size = if image.width() < 200 { 
                Size::new(550, 700)
            } else if image.height() > 768 {
                Size::new(image.width()/2, image.height()/2)
            } else {
                Size::new(image.width(), image.height())
            };
            (folder.len(), Some(current_index), Some(folder), image_size)
        };
        create_config_dir();
        let path_to_settings_file = create_settings_file().to_str().unwrap().to_string();

        (   
            Liv {
                current_image: current_image,
                current_folder: current_folder,
                folder_length: folder_length,
                is_hidden: true,
                settings: manager::settings_parser::settings_parser(path_to_settings_file.clone()),
                path_to_settings_file: path_to_settings_file,
            },
            iced::window::resize(image_size)
        )
    }

    fn update(&mut self, message: Self::Message) -> Command<Self::Message> {
        match message {
            Message::Close => {
                return window::close::<Message>();
            },
            Message::ChangeImage(source) => {
                self.current_image = Some(source);
            },
            Message::DragWindow => {
                return window::drag();
            },
            Message::ToggleModal => {
                self.is_hidden = !self.is_hidden;
            },
            Message::ToggleTitlebar(is_hide) => {
                self.settings.titlebar_hide = is_hide;
                
                let _ = manager::files::toggle_property(manager::files::Toggle::TitleBar, self.path_to_settings_file.clone());
            },
            Message::ToggleImageInDirectory(event) => {
                match event{
                    Event::Keyboard(
                        iced::keyboard::Event::KeyPressed{
                            key_code: iced::keyboard::KeyCode::Left,
                            modifiers: _
                        }
                    ) => {
                        if self.current_folder.is_some() {
                            let current_image = self.current_image.as_ref().unwrap();
                            
                            let image_num = if current_image == &0 {
                                self.folder_length - 1
                            } else {
                                current_image - 1
                            };

                            self.current_image = Some(image_num.clone());
                        }
                    },
                    Event::Keyboard(
                        iced::keyboard::Event::KeyPressed{
                            key_code: iced::keyboard::KeyCode::Right,
                            modifiers: _
                        }
                    ) => {
                        if self.current_folder.is_some() {
                            let current_image = self.current_image.as_ref().unwrap();
                            
                            let image_num = if current_image == &(self.folder_length - 1){
                                0
                            } else {
                                current_image + 1
                            };
                            self.current_image = Some(image_num.clone());
                        }
                    },
                    _ => {}
                }
            },
        }
        Command::none()
    }

    fn view(&self) -> iced::Element<Message> {
        let source = match &self.current_image {
            Some(src) => {
                self.current_folder.as_ref().unwrap()[*src].clone()
            },
            None => {
                String::from("/home/rscasm/Загрузки/run.png")
            }
        };

        let modal_overlay = if self.is_hidden {
            None
        } else {
            Some(
                container(
                    column!(
                        row!(
                            toggler(String::from("Показывать скрытые"), self.settings.titlebar_hide, |b| Message::ToggleTitlebar(b))
                            .width(Length::Fill),
                        ),
                    )
                )
                .height(500.0)
                .width(600.0)
                .center_x()
                .center_y()
                .style(iced::theme::Container::Custom(Box::new(themes::containers::ContainerTheme)))
            )
        };
        let main = floating_element(
            column!(
                container(
                    Viewer::new(Handle::from_path(source))
                    .width(Length::Fill)
                    .height(Length::Fill)
                    .padding(5)
                )
                .width(Length::Fill)
                .height(Length::Fill)
                .center_x()
                .center_y()
                .style(iced::theme::Container::Custom(Box::new(themes::containers::ContainerTheme)))
            )
            .width(Length::Fill)
            .height(Length::Fill),
            widgets::titlebar::new()
        )
        .anchor(Anchor::North);
        container(
            modal(main, modal_overlay)
            .backdrop(Message::ToggleModal)
            .on_esc(Message::ToggleModal)
            .style(iced_aw::style::ModalStyles::custom(themes::modal::ModalTheme))
        )
        .width(Length::Fill)
        .height(Length::Fill)
        .style(iced::theme::Container::Custom(Box::new(themes::containers::ContainerTheme)))
        .into()
    }

    fn title(&self) -> String {
        String::from("Left image viewer")
    }

    fn style(&self) -> iced::theme::Application{
        iced::theme::Application::Custom(Box::new(crate::themes::application::ApplicationTheme))
    }

    fn subscription(&self) -> Subscription<Message> {
        Subscription::from(
            subscription::events().map(Message::ToggleImageInDirectory),
        )
    }
}

fn main() -> iced::Result {
    Liv::run(Settings {
        window: window::Settings {
            decorations: false,
            resizable: true,
            size: (500,300),
            transparent: true,
            ..window::Settings::default()
        },
        ..Settings::default()
    })
}
