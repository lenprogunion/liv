use iced::{self, 
    widget::{
        container, Container, 
        row, 
        button, mouse_area, 
        image,
        Space, MouseArea, 
    }, 
    Length, Renderer
};
use iced_aw::{FloatingElement, floating_element};

use crate::{Message, themes};

pub fn new() -> MouseArea<'static, Message, Renderer> {
    mouse_area(
        row!(
            Space::new(Length::Fill, Length::Fixed(26.0)),
            button(image(""))
            .width(26)
            .height(26)
            .on_press(Message::ToggleModal)
            .style(iced::theme::Button::Custom(Box::new(themes::buttons::TitleBarMenuButton))),
            button(image(""))
            .width(26)
            .height(26)
            .on_press(Message::Close)
            .style(iced::theme::Button::Custom(Box::new(themes::buttons::TitleBarButton)))
        )
        .spacing(10)
        .padding(13)
        .width(Length::Fill)
        .align_items(iced::Alignment::End)
    )
    .on_press(Message::DragWindow)
}