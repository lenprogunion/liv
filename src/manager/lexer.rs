#[derive(Clone, Debug)]
pub enum Token{
    Directive(String),
    Tag{
        name: String,
        value: String,
    },
}

pub fn lexer(text: String) -> Vec<Token>{
    let mut tokens = Vec::<Token>::new();
    let mut text = text.chars();
    let mut word = String::new();
    let mut is_directive = false;
    let mut tag_name = String::new();
    let mut is_tag_value = false;
    let mut is_comment = false;
    while let Some(symbol) = text.next(){
        match symbol {
            '#' => {
                is_comment = true;
            },
            '[' => {
                if is_comment{ continue; }
                if !word.is_empty(){
                    continue;
                }
                is_directive = true;
            },
            ']' => {
                if is_comment{ continue; }
                if !is_directive{
                    continue;
                }
                tokens.push(Token::Directive(word.clone()));
                word.clear();
                is_directive = false;
            },
            '=' => {
                if is_comment{ continue; }
                if is_tag_value{ 
                    continue; 
                }
                tag_name = word.clone();
                is_tag_value = true;
                word.clear();
            },
            '\n' => {
                is_comment = false;
                tokens.push(Token::Tag{
                    name: tag_name.clone(),
                    value: word.clone(),    
                });
                tag_name.clear();
                word.clear();
                is_tag_value = false;
            },
            _ => {
                word.push(symbol);
            }
        }
    }
    tokens
}