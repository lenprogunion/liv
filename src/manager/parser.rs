use std::fs;

use super::lexer::{Token, lexer};

#[derive(Clone, Default)]
pub struct DesktopStruct{
    pub file_name: String,
    pub name: String,
    pub exec: String,
    pub icon: String,
    pub flatpak: String,
    pub nodisplay_iced: bool,
    pub nodisplay: bool,
    pub on_panel: bool,
    pub is_system: bool
}

impl DesktopStruct {
    fn default() -> DesktopStruct{
        DesktopStruct{
            file_name: String::new(),
            name: String::new(),
            exec: String::new(),
            icon: String::new(),
            on_panel: false,
            flatpak: String::new(),
            nodisplay_iced: false,
            nodisplay: false,
            is_system: false
        }
    }
}

pub fn desktop_parser(with_hide: bool, path: String) -> (Vec<DesktopStruct>, Vec<DesktopStruct>){
    let paths = fs::read_dir(&path.clone()).unwrap();
    let mut apps = Vec::<DesktopStruct>::new();
    let mut panel_apps = Vec::<DesktopStruct>::new();
    for path_entry in paths {
        if let Ok(entry) = path_entry{
            if let Some(extension) = entry.path().extension() {
                if extension == "desktop"{
                    let file = std::fs::read(entry.path());
                    let data_from_file = String::from_utf8(file.unwrap()).unwrap();
                    let tokens = lexer(data_from_file);
                    let mut data_from_tokens = parse_tokens(tokens);
                    data_from_tokens.file_name = entry.file_name().to_str().unwrap().to_string();
                    if data_from_tokens.nodisplay { continue; }
                    if data_from_tokens.nodisplay_iced && !with_hide{ continue; } 
                    if data_from_tokens.on_panel{
                        panel_apps.push(data_from_tokens);
                    } else{
                        apps.push(data_from_tokens);
                    }
                }
            }
        }
    }
    (apps, panel_apps)
}

fn parse_tokens(tokens: Vec<Token>) -> DesktopStruct{
    let mut is_directive = true;
    let mut result = DesktopStruct::default();

    for token in tokens.iter(){
        match token{
            Token::Tag { name, value } => {
                if name.is_empty() || value.is_empty(){ continue; };
                match name.as_str(){
                    "Exec" => {
                        if !is_directive{ continue; }
                        result.exec = value.to_string();
                    },
                    "Name" => {
                        if !is_directive{ continue; }
                        result.name = value.to_string();
                    },
                    "Icon" => {
                        if !is_directive{ continue; }
                        result.icon = value.to_string();
                    },
                    "X-Flatpak" => {
                        if !is_directive{ continue; }
                        result.flatpak = value.to_string();
                    },
                    "X-NoDisplay-iced" => {
                        if value.contains("true"){
                            result.nodisplay_iced = true;
                        }
                    },
                    "NoDisplay" => {
                        if value.contains("true"){
                            result.nodisplay = true;
                        }
                    },
                    "OnPanel" => {
                        if value.contains("true"){
                            result.on_panel = true;
                        }
                    },
                    "StartupNotify" => {
                        if value.contains("false"){
                            result.is_system = true;
                        }
                    }
                    _ => {
                        
                    }
                }
            },
            Token::Directive(name) => {
                if name != "Desktop Entry"{
                    is_directive = false;
                }
            }
        }
    }
    result 
}

